# SMART_PLUG

Conventional plug which we use in our households to be controlled manually. But nowadays there are SmartPlug available in the market whose readings can be monitored and controlled from anywhere using the internet and not only energy consumption but we can monitor multiple parameters such as voltage, current, power etc. on laptop or mobile using IoT.
![smartplug](https://gitlab.com/anooja.sk/smart_plug/-/raw/e7488a142160c92f2ce56b16ec411d820e2a8c72/IMAGES/IMG_20220702_113009_HDR.jpg)

# List of Parameters recorded
- Measuring Power, Voltage and Current
- Voltage Measuring Range: 80-260 VAC
- Current Measuring Range: 0-100 Amp
- Active Power Measuring Range: 0-23KW
- Time scheduling
- ON/OFF controlling


# Prerequisites

- HARD WARE
- Node MCUESP32 (WROOM32)
- HI_LINK
- PZEM-004T
- CT Coil
- Relay Module
- Jumpers
- Connecting Wire

# SOFTWARE SUPPORT
- Arduino IDE
- Mqtt client and subscriber
- Node_red

# Getting Started

- Make sure that you have a NodeMCU ESPWROOM32
- Install Arduino IDE 
- To upload code into NodeMCU using Arduino IDE
- Connect the components as shown in the Wiring Diagram

# License
 This project is licensed under the MIT License - see the LICENSE.md file for details

